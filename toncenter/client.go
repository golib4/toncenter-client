package toncenter

import (
	"errors"
	"fmt"
	"gitlab.com/golib4/http-client/http"
)

type Client struct {
	httpClient *http.Client
	apiKey     string
}

func NewClient(httpClient *http.Client, apiKey string) Client {
	return Client{
		httpClient: httpClient,
		apiKey:     apiKey,
	}
}

type TransactionsRequest struct {
	Account      string `schema:"account"`
	Limit        int32  `schema:"limit"`
	MinTimestamp int64  `schema:"start_utime"`
}

type Transaction struct {
	Account     string       `json:"account"`
	Hash        string       `json:"hash"`
	Now         int64        `json:"now"`
	InMsg       *InMsg       `json:"in_msg"`
	Description *Description `json:"description"`
	OutMsgs     []OutMsg     `json:"out_msgs"`
	BlockRef    BlockRef     `json:"block_ref"`
}

type Action struct {
	ResultCode *int `json:"result_code"`
}

type Description struct {
	Action    *Action    `json:"action"`
	ComputePH *ComputePH `json:"compute_ph"`
}

type BlockRef struct {
	Workchain int `json:"workchain"`
}

type InMsg struct {
	Source      *string `json:"source"`
	Destination string  `json:"destination"`
	Value       *string `json:"value"`
}

type MsgContent struct {
	Hash string `json:"hash"`
	Body string `json:"body"`
}

type OutMsg struct {
	Hash        string `json:"hash"`
	Source      string `json:"source"`
	Destination string `json:"destination"`
	Value       string `json:"value"`
}

type ComputePH struct {
	ExitCode *int `json:"exit_code"`
}

type TransactionsResponse struct {
	Transactions []Transaction `json:"transactions"`
}

func (c Client) GetAccountTransactions(request TransactionsRequest) (*TransactionsResponse, error) {
	var response TransactionsResponse
	httpError, err := c.httpClient.SendGetRequest(http.SchemaRequestData{
		Body: request,
		Data: http.RequestData{
			Path:    "/api/v3/transactions",
			Headers: []http.HeaderData{{Name: "X-API-Key", Value: c.apiKey}},
		},
	}, &response)
	if httpError != nil {
		return nil, fmt.Errorf("erro while trying to get transactions in toncenter: %s", httpError)
	}
	if err != nil {
		return nil, fmt.Errorf("erro while trying to get transactions in toncenter: %s", err)
	}

	return &response, nil
}

type GetAddressInfoRequest struct {
	Address string `schema:"address"`
}

type GetAddressInfoResponse struct {
	Balance             string `json:"balance"`
	WalletType          string `json:"wallet_type"`
	Seqno               int    `json:"seqno"`
	WalletID            int    `json:"wallet_id"`
	LastTransactionLT   string `json:"last_transaction_lt"`
	LastTransactionHash string `json:"last_transaction_hash"`
	Status              string `json:"status"`
}

func (c Client) GetAddressInfo(request GetAddressInfoRequest) (*GetAddressInfoResponse, error) {
	var response GetAddressInfoResponse
	httpError, err := c.httpClient.SendGetRequest(http.SchemaRequestData{
		Body: request,
		Data: http.RequestData{
			Path:    "/api/v3/wallet",
			Headers: []http.HeaderData{{Name: "X-API-Key", Value: c.apiKey}},
		},
	}, &response)
	if err != nil {
		return nil, fmt.Errorf("error while doing toncenter address info request: %s", err)
	}
	if httpError != nil {
		return nil, fmt.Errorf("error while doing toncenter address info request: %s", httpError)
	}

	return &response, nil
}

type GetAddressBalanceResponse struct {
	Result interface{} `json:"result"`
}

type GetAddressBalanceRequest struct {
	Address string
}

func (c Client) GetAddressBalance(request GetAddressBalanceRequest) (*GetAddressBalanceResponse, error) {
	var response GetAddressBalanceResponse
	httpError, err := c.httpClient.SendGetRequest(http.SchemaRequestData{
		Data: http.RequestData{
			Path:    fmt.Sprintf("/api/v2/getAddressBalance?address=%s", request.Address),
			Headers: []http.HeaderData{{Name: "X-API-Key", Value: c.apiKey}},
		},
	}, &response)
	if err != nil {
		return nil, fmt.Errorf("error while doing toncenter request: %s", err)
	}
	if httpError != nil {
		return nil, fmt.Errorf("error while doing toncenter request: %s", httpError)
	}

	return &response, nil
}

type SendMessageRequest struct {
	Message string `json:"boc"`
}

type SendMessageResponse struct {
	Ok     bool `json:"ok"`
	Result *struct {
		Hash string `json:"hash"`
	}
	Error *string `json:"error"`
}

var RateLimitError = errors.New("rate limiter error")

func (c Client) SendMessage(request SendMessageRequest) (*SendMessageResponse, error) {
	var response SendMessageResponse
	httpError, err := c.httpClient.SendJsonPostRequest(http.JsonRequestData{
		Body: request,
		Data: http.RequestData{
			Path:    "/api/v2/sendBocReturnHash",
			Headers: []http.HeaderData{{Name: "X-API-Key", Value: c.apiKey}},
		},
	}, &response)
	if httpError != nil {
		if httpError.Status == "429 Too Many Requests" {
			return nil, RateLimitError
		}
		return nil, fmt.Errorf("erro while trying to transfer in toncenter: %s", httpError)
	}
	if err != nil {
		return nil, fmt.Errorf("erro while trying to transfer in toncenter: %s", httpError)
	}
	if !response.Ok {
		return nil, fmt.Errorf("erro while trying to transfer in toncenter: %s", *response.Error)
	}

	return &response, nil
}

type RunGetMethodRequest struct {
	Address string      `json:"address"`
	Method  string      `json:"method"`
	Stack   []StackItem `json:"stack"`
}

type StackItem struct {
	Type  string `json:"type"`
	Value string `json:"value"`
}

type RunGetMethodResponse struct {
	GasUsed  int         `json:"gas_used"`
	ExitCode int         `json:"exit_code"`
	Stack    []StackItem `json:"stack"`
}

func (c Client) RunGetMethod(request RunGetMethodRequest) (*RunGetMethodResponse, error) {
	var response RunGetMethodResponse
	httpError, err := c.httpClient.SendJsonPostRequest(http.JsonRequestData{
		Body: request,
		Data: http.RequestData{
			Path:    "/api/v3/runGetMethod",
			Headers: []http.HeaderData{{Name: "X-API-Key", Value: c.apiKey}},
		},
	}, &response)
	if httpError != nil {
		if httpError.Status == "429 Too Many Requests" {
			return nil, RateLimitError
		}
		return nil, fmt.Errorf("erro while trying to RunGetMethod in toncenter: %s", httpError)
	}
	if err != nil {
		return nil, fmt.Errorf("erro while trying to RunGetMethod in toncenter: %s", httpError)
	}

	return &response, nil
}

type GetJettonTransfersRequest struct {
	OwnerAddress          string `schema:"address"`
	JettonContractAddress string `schema:"jetton_master"`
	MinTimestamp          int64  `schema:"start_utime"`
	Limit                 int32  `schema:"limit"`
}

type JettonTransfer struct {
	QueryID             string `json:"query_id"`
	Source              string `json:"source"`
	Destination         string `json:"destination"`
	Amount              string `json:"amount"`
	SourceWallet        string `json:"source_wallet"`
	JettonMaster        string `json:"jetton_master"`
	TransactionHash     string `json:"transaction_hash"`
	TransactionLT       string `json:"transaction_lt"`
	TransactionNow      int64  `json:"transaction_now"`
	ResponseDestination string `json:"response_destination"`
	ForwardTonAmount    string `json:"forward_ton_amount"`
}

type GetJettonTransfersResponse struct {
	JettonTransfers []JettonTransfer `json:"jetton_transfers"`
}

func (c Client) GetJettonTransfers(request GetJettonTransfersRequest) (*GetJettonTransfersResponse, error) {
	var response GetJettonTransfersResponse
	httpError, err := c.httpClient.SendGetRequest(http.SchemaRequestData{
		Body: request,
		Data: http.RequestData{
			Path:    "/api/v3/jetton/transfers",
			Headers: []http.HeaderData{{Name: "X-API-Key", Value: c.apiKey}},
		},
	}, &response)
	if err != nil {
		return nil, fmt.Errorf("error while doing toncenter GetJettonTransfers request: %s", err)
	}
	if httpError != nil {
		return nil, fmt.Errorf("error while doing toncenter GetJettonTransfers request: %s", httpError)
	}

	return &response, nil
}

type GetJettonWalletRequest struct {
	JettonWalletAddress string `schema:"address"`
}

type JettonWallet struct {
	Address           string `json:"address"`
	Balance           string `json:"balance"`
	Owner             string `json:"owner"`
	Jetton            string `json:"jetton"`
	LastTransactionLT string `json:"last_transaction_lt"`
	CodeHash          string `json:"code_hash"`
	DataHash          string `json:"data_hash"`
}

type GetJettonWalletResponse struct {
	JettonWallets []JettonWallet `json:"jetton_wallets"`
}

func (c Client) GetJettonWallet(request GetJettonWalletRequest) (*GetJettonWalletResponse, error) {
	var response GetJettonWalletResponse
	httpError, err := c.httpClient.SendGetRequest(http.SchemaRequestData{
		Body: request,
		Data: http.RequestData{
			Path:    "/api/v3/jetton/wallets",
			Headers: []http.HeaderData{{Name: "X-API-Key", Value: c.apiKey}},
		},
	}, &response)
	if err != nil {
		return nil, fmt.Errorf("error while doing toncenter GetJettonWallet request: %s", err)
	}
	if httpError != nil {
		return nil, fmt.Errorf("error while doing toncenter GetJettonWallet request: %s", httpError)
	}

	return &response, nil
}

type GetTransactionByHashRequest struct {
	Hash      string `schema:"msg_hash"`
	Direction string `schema:"direction"`
}

func (c Client) GetTransactionByHash(request GetTransactionByHashRequest) (*TransactionsResponse, error) {
	var response TransactionsResponse
	httpError, err := c.httpClient.SendGetRequest(http.SchemaRequestData{
		Body: request,
		Data: http.RequestData{
			Path:    "/api/v3/transactionsByMessage",
			Headers: []http.HeaderData{{Name: "X-API-Key", Value: c.apiKey}},
		},
	}, &response)
	if httpError != nil {
		return nil, fmt.Errorf("erro while trying to get transactionsByMessage in toncenter: %s", httpError)
	}
	if err != nil {
		return nil, fmt.Errorf("erro while trying to get transactionsByMessage in toncenter: %s", err)
	}

	return &response, nil
}

type EstimateFeeRequest struct {
	Body    string `json:"body"`
	Address string `json:"address"`
}

type EstimateFeeResponse struct {
	Result struct {
		SourceFees struct {
			InFwdFee   int `json:"in_fwd_fee"`
			StorageFee int `json:"storage_fee"`
			GasFee     int `json:"gas_fee"`
			FwdFee     int `json:"fwd_fee"`
		} `json:"source_fees"`
	} `json:"result"`
}

func (c Client) EstimateFee(request EstimateFeeRequest) (*EstimateFeeResponse, error) {
	var response EstimateFeeResponse
	httpError, err := c.httpClient.SendJsonPostRequest(http.JsonRequestData{
		Body: request,
		Data: http.RequestData{
			Path:    "/api/v2/estimateFee",
			Headers: []http.HeaderData{{Name: "X-API-Key", Value: c.apiKey}},
		},
	}, &response)
	if httpError != nil {
		return nil, fmt.Errorf("erro while trying to estimate in toncenter: %s", httpError)
	}
	if err != nil {
		return nil, fmt.Errorf("erro while trying to estimate in toncenter: %s", httpError)
	}

	return &response, nil
}
